import axios, {AxiosError} from 'axios';
import { execSync } from 'child_process';

const API_URL = 'https://api.devcycle.com/';
const AUTH_URL = 'https://auth.devcycle.com/';
const DVC_IDENTIFIER = 'gitlab.code_usages';

// Run the 'dvc usages' command and parse its output into a JSON object
export const run = async () => {
    execSync('npm install -g @devcycle/cli@5.10.1')
    const variablesString = execSync(
        `dvc usages --format json --caller ${DVC_IDENTIFIER}`,
        { encoding: 'utf8' }
    );
    const variables = JSON.parse(variablesString);
    await postCodeUsages(variables);
}

export const authenticate = async (client_id: string, client_secret: string): Promise<string> => {
    const url = new URL('/oauth/token', AUTH_URL);
    try {
        const response = await axios.post(url.href, {
            grant_type: 'client_credentials',
            client_id,
            client_secret,
            audience: 'https://api.devcycle.com/',
        });

        return response.data.access_token;
    } catch (e) {
        throw new Error('Failed to authenticate with the DevCycle API. Check your credentials.');
    }
}

export const postCodeUsages = async (variables: any[]): Promise<void> => {
    const projectKey = process.env.DEVCYCLE_PROJECT_KEY || process.env.DVC_PROJECT_KEY;
    const clientSecret = process.env.DEVCYCLE_CLIENT_SECRET || process.env.DVC_CLIENT_SECRET;
    const clientId = process.env.DEVCYCLE_CLIENT_ID || process.env.DVC_CLIENT_ID;

    if (!clientSecret || !clientId || !projectKey) {
        throw new Error('Required environment variables are not set.');
    }

    const authToken = await authenticate(clientId, clientSecret);
    const url = new URL(`/v1/projects/${projectKey}/codeUsages`, API_URL);

    const headers = { Authorization: authToken, 'dvc-referrer': DVC_IDENTIFIER };
    const branch = process.env.CI_COMMIT_REF_NAME;
    const repo = process.env.CI_PROJECT_PATH;

    if (!branch || !repo) {
        throw new Error('Failed to retrieve branch or repo name.');
    }

    try {
        await axios.post(
            url.href,
            {
                source: 'gitlab',
                repo: `${repo}`,
                branch: branch.split('/').pop(),
                variables
            },
            { headers }
        );
        console.log('Successfully uploaded your code usages to DevCycle!');
    } catch (e) {
        let errorMessage = 'Failed to submit Code Usages with error:';
        if (e instanceof Error) {
            errorMessage += e.message;
            if ('isAxiosError' in e) {
                const axiosError = e as AxiosError;
                errorMessage += "\nError Details:" +
                    "\nData: " + JSON.stringify(axiosError.response?.data)
                    "\nStatus: " + (axiosError.response?.status || 'N/A') +
                    "\nHeaders: " + JSON.stringify(axiosError.response?.headers);
            }
        } else {
            errorMessage += JSON.stringify(e);
        }
        throw new Error(errorMessage);
    }
}
