import * as action from '../src/postCodeUsages'
import axios from 'axios';

jest.mock('axios')

const mockInputs: Record<string, string> = {
    'DEVCYCLE_PROJECT_KEY': 'my-project',
    'DEVCYCLE_CLIENT_ID': 'id',
    'DEVCYCLE_CLIENT_SECRET': 'secret'
}

describe('run', () => {
    beforeAll(() => {
        jest.spyOn(action, 'authenticate').mockResolvedValue('generated-token')
    })

    afterAll(() => {
        jest.restoreAllMocks()
    })

    beforeEach(() => {
        jest.clearAllMocks()
        process.env = Object.assign(process.env, {...mockInputs});
        process.env.CI_COMMIT_REF_NAME = 'main'
        process.env.CI_PROJECT_PATH = 'my-repo';
    })

    test.each([
        'DEVCYCLE_PROJECT_KEY', 'DEVCYCLE_CLIENT_ID', 'DEVCYCLE_CLIENT_SECRET'
    ])('fails when missing environment variable: %s', async (param) => {
        delete process.env[param]
        await expect(action.postCodeUsages([])).rejects.toThrow('Required environment variables are not set.')
    })

    it('calls postCodeUsages for a valid request', async () => {
        (axios.post as jest.MockedFunction<typeof axios.post>).mockResolvedValue({ data: { access_token: '123' } })
        await action.postCodeUsages([])
        expect(action.authenticate).toBeCalledTimes(1)
        expect(action.authenticate).toHaveBeenCalledWith(mockInputs.DEVCYCLE_CLIENT_ID, mockInputs.DEVCYCLE_CLIENT_SECRET)
        expect(axios.post).toBeCalledWith(
            `https://api.devcycle.com/v1/projects/${mockInputs.DEVCYCLE_PROJECT_KEY}/codeUsages`,
            expect.objectContaining({
                "source": "gitlab",
                "repo": process.env.CI_PROJECT_PATH,
                "branch": process.env.CI_COMMIT_REF_NAME,
                "variables": []
            }),
            expect.objectContaining({
                headers: {
                    Authorization: 'generated-token',
                    'dvc-referrer': 'gitlab.code_usages'
                }
            })
        )
    })

    it('fails if an error is thrown when sending code usages', async () => {
        (axios.post as jest.MockedFunction<typeof axios.post>).mockRejectedValue({
            response: { data: { message: 'Some error' } }
        })

        const postCodeUsages = () => action.postCodeUsages([])

        await expect(postCodeUsages).rejects.toThrow('Failed to submit Code Usages with error:{\"response\":{\"data\":{\"message\":\"Some error\"}}}')
    })
})

describe('authenticate', () => {
    beforeEach(() => {
        jest.clearAllMocks()
    })

    it('sends authentication request', async () => {
        (axios.post as jest.MockedFunction<typeof axios.post>).mockResolvedValue({ data: { access_token: '123' } })

        const returnedToken = await action.authenticate('mock-client-id', 'mock-client-secret')

        expect(axios.post).toBeCalledWith(
            'https://auth.devcycle.com/oauth/token',
            expect.objectContaining({
                grant_type: 'client_credentials',
                client_id: 'mock-client-id',
                client_secret: 'mock-client-secret',
                audience: 'https://api.devcycle.com/',
            })
        )
        expect(returnedToken).toEqual('123')
    })

    it('fails if an error is thrown during authentication', async () => {
        (axios.post as jest.MockedFunction<typeof axios.post>).mockRejectedValue('Some error')

        const authenticate = () => action.authenticate('mock-client-id', 'mock-client-secret')

        await expect(authenticate).rejects.toThrow('Failed to authenticate with the DevCycle API. Check your credentials.')
    })
})
