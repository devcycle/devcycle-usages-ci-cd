## Overview
With this GitLab CI/CD pipeline, your [DevCycle](https://devcycle.com/) dashboard will be updated to display code snippets for each DevCycle variable usage within your project.

Note: This is intended to run when pushing changes to your main branch.

### Example Output

![Example Output](https://gitlab.com/devcycle/devcycle-usages-ci-cd/-/raw/main/example_output.png)

### Usage
Create a new `.gitlab-ci.yml` file in your GitLab repository or update the existing one. Add the code_usages stage and paste the following code into a code_usages:

```yaml
stages:
  - code_usages

code_usages:
  stage: code_usages
  image: devcycleinfra/devcycle-code-refs-gitlab:latest
  script:
    - node /devcycle-usages-ci-cd
  only:
    - main
```

Your DevCycle API credentials and project token are required to update the DevCycle dashboard.

When referencing your API client ID and secret, we recommend using GitLab CI/CD variables to store your credentials securely.
## Environment Variables

Your DevCycle API credentials and project token are required to update the DevCycle dashboard. These should be set as environment variables in your GitLab project settings:

| Variable | Description |
| -------- | ----------- |
| `DEVCYCLE_PROJECT_KEY` | Your DevCycle project key, see [Projects](https://app.devcycle.com/r/projects) |
| `DEVCYCLE_CLIENT_ID` | Your organization's API client ID, see [Organization Settings](https://app.devcycle.com/r/settings) |
| `DEVCYCLE_CLIENT_SECRET` | Your organization's API client secret, see [Organization Settings](https://app.devcycle.com/r/settings) |

When setting these environment variables, we recommend you protect them to ensure they're only exposed to protected branches or tags and mask them to hide their values in job logs.

## Usage

1. Set the necessary environment variables in your GitLab project settings as described above.
2. Add the provided `.gitlab-ci.yaml` to your project root.
3. Push your changes. The pipeline should run automatically when you push changes to your `main` branch.

## Support

For any issues, feedback, or questions, please feel free to open an issue on this repository.
