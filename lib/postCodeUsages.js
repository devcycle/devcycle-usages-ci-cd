"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postCodeUsages = void 0;
const axios_1 = __importDefault(require("axios"));
const API_URL = 'https://api.devcycle.com/';
const AUTH_URL = 'https://auth.devcycle.com/';
const DVC_IDENTIFIER = 'gitlab.code_usages';
const projectKey = process.env.DVC_PROJECT_KEY;
const clientSecretString = process.env.DVC_CLIENT_SECRET;
const variablesString = process.env.VARIABLES;
const clientIdString = process.env.DVC_CLIENT_ID;
if (!variablesString) {
    throw new Error('VARIABLES environment variable is not set.');
}
if (!clientIdString) {
    throw new Error('VARIABLES environment variable is not set.');
}
if (!clientSecretString) {
    throw new Error('VARIABLES environment variable is not set.');
}
const variables = JSON.parse(variablesString);
const clientId = JSON.parse(clientIdString);
const clientSecret = JSON.parse(clientSecretString);
const authenticate = (client_id, client_secret) => __awaiter(void 0, void 0, void 0, function* () {
    const url = new URL('/oauth/token', AUTH_URL);
    try {
        const response = yield axios_1.default.post(url.href, {
            grant_type: 'client_credentials',
            client_id,
            client_secret,
            audience: 'https://api.devcycle.com/',
        });
        return response.data.access_token;
    }
    catch (e) {
        throw new Error('Failed to authenticate with the DevCycle API. Check your credentials.');
    }
});
const postCodeUsages = (variables) => __awaiter(void 0, void 0, void 0, function* () {
    const authToken = yield authenticate(clientId, clientSecret);
    const url = new URL(`/v1/projects/${projectKey}/codeUsages`, API_URL);
    const headers = { Authorization: authToken, 'dvc-referrer': DVC_IDENTIFIER };
    const branch = process.env.CI_COMMIT_REF_NAME;
    const repo = process.env.CI_PROJECT_PATH;
    if (!branch || !repo) {
        throw new Error('Failed to retrieve branch or repo name.');
    }
    try {
        yield axios_1.default.post(url.href, {
            source: 'gitlab',
            repo: `${repo}`,
            branch: branch.split('/').pop(),
            variables
        }, { headers });
        console.log('Successfully uploaded your code usages to DevCycle!');
    }
    catch (e) {
        throw new Error('Failed to submit Code Usages.');
    }
});
exports.postCodeUsages = postCodeUsages;
(0, exports.postCodeUsages)(variables);
